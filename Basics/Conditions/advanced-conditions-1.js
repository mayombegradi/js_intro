let isAccountLocked = true
let userRole = 'admin'

if (!isAccountLocked && userRole == 'admin') {
    console.log(`Welcome ${userRole}`)
} else if (!isAccountLocked) {
    console.log('Welcome')
} else {
    console.log('Account is locked')
}