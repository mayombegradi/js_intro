// Variables are camelCased in JavaScript!
let firstName = 'John'
let lastName = 'Doe'

// Concatenation operator `+`
let fullName = firstName + ' ' + lastName

// JS relies on automatic semicolon insertion!
// No need to end every statement with a `;`
console.log(fullName)