// Variables:
// city
// Country
// location: city, country

let city = 'Johannesburg'
let country = 'South Africa'

let location = city + ', ' + country

// String interpolation `${variable}`
console.log(`I live in: ${location}`)