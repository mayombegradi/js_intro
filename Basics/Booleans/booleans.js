let temp = 31
let isFreezing

isFreezing = temp === 31 // true
console.log(isFreezing)

isFreezing = temp !== 30 // true
console.log(isFreezing)

isFreezing = temp === 30 // false
console.log(isFreezing)

isFreezing = temp <= 32  // true
console.log(isFreezing)

isFreezing = temp >= 32  // false
console.log(isFreezing)

isFreezing = temp > 32   // false
console.log(isFreezing)

isFreezing = temp < 32   // true
console.log(isFreezing)