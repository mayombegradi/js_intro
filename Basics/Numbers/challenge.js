// Convert student's score into percentage.
// variables: maximum score & student's obtained score.

let maxScore = 20
let studentScore = 18

let percentage = (studentScore * 100) / maxScore
console.log(`The student obtained: ${percentage}%`)