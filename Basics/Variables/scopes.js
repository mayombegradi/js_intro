let varOne = 'varOne' // Global scope

if (true) {
    let varTwo = 'VarTwo' // Local scope

    console.log(varOne)
    if (true) {
        console.log(varTwo)
    }
}

// The below raises an error!
// console.log(varTwo)