let petName = 'Rocky'
petName = 'Roxan' // Variable reassignment.

// The below is invalid because it's already defined
// let petName = 'Roxan'

// Variable name rules:
// Start with a lowercase letter (a-z) or `$` or `_`;
// Contain only alphanumeric characters and `_`
// Cannot be reserved keywords.