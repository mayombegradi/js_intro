const todos = ['Order cat food', 'Clean kitchen', 'Buy food', 'Do work', 'Exercise']

console.log(`You have ${todos.length} todos!`)

todos.forEach(function (todo, index) {
    console.log(`${index+1}. ${todo}`)
})