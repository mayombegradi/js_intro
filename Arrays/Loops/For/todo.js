const todos = ['Order cat food', 'Clean kitchen', 'Buy food', 'Do work', 'Exercise']

console.log(`You have ${todos.length} todos!`)

for (let index = todos.length - 1; index >= 0; index--)
    console.log(`${index + 1}. ${todos[index]}`)