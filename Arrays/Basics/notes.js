const notes = ['Note 1', 'Note 2',  'Note 3']

console.log(`[${notes}] has a length of ${notes.length}`)
console.log(`And its first element is '${notes[0]}'`)
console.log(`Its last element is '${notes[notes.length-1]}'`)