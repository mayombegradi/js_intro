const notes = ['Note 1', 'Note 2',  'Note 3']

notes.push('Note 4')
console.log(notes)

console.log(`Popped value is: ${notes.pop()}`)
console.log('New notes list is: ', notes)

console.log(notes.shift())
console.log(notes)

console.log(notes.unshift('Note 1'))
console.log(notes)

// Adds a new note at index one
// notes.splice(1, 0, 'New note')

// Replaces element at index 1 with `New note`
// Same as notes[1] = 'New note'
// notes.splice(1, 1, 'New note')