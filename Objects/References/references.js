let myAccount = {
    name: 'Andrew Mead',
    expenses: 0,
    income: 0
}

let otherAccount = myAccount // These 2 objects are bound
otherAccount.income = 1000

let addExpense = function (account, amount = 0) {
    // account.expenses += amount
    account = { expenses: amount } // This breaks the binding
    console.log(account)
}

addExpense(myAccount, 2.5)
console.log(myAccount)