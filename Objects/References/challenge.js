let myAccount = {
    name: 'Andrew Mead',
    expenses: 0,
    income: 0
}

let addIncome = function (account, amount) {
    account.income += amount
}

let addExpenses = function (account, amount) {
    account.expenses += amount
}

let resetAccount = function (account) {
    account.expenses = 0,
        account.income = 0
}

let getAccountSummary = function (account) {
    let balance = account.income - account.expenses
    return `Account for ${account.name} has $${balance}. $${account.income} in income.` +
        ` $${account.expenses} in expenses.`
}

addIncome(myAccount, 100)
addExpenses(myAccount, 2.5)

console.log(getAccountSummary(myAccount))

resetAccount(myAccount)
console.log(getAccountSummary(myAccount))
