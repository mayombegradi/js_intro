const num = 103.941

console.log(num.toFixed(2)) // 103.94

console.log(Math.round(num)) // 104
console.log(Math.floor(num)) // 103
console.log(Math.ceil(num))  // 104

const min = 10
const max = 20
const randomNum = Math.floor(Math.random() * (max-min+1)) + min

console.log(randomNum)