let name = 'Andrew Mead'

// Length property
console.log(`The length of ${name} is: ${name.length}`)

// Convert to upper case
let upperCaseName = name.toUpperCase()
let lowerCaseName = name.toLowerCase()
console.log(upperCaseName)
console.log(lowerCaseName)

// name: 'Andrew Mead'. Strings are immutable
console.log(name)

const sentence = 'The quick brown fox jumps over the lazy dog.';
const word = 'fox';

console.log(`The word "${word}" ${sentence.includes(word) ? 'is' : 'is not'} in the sentence`)

// isValidPassword
let isValidPassword = function (password) {
    let condition = (password.length > 8 && !password.includes('password'))
    console.log(`${password} ${condition ? 'is': 'is not'} valid`)
}

isValidPassword('asdfp')
isValidPassword('abc123!@#$%^&')
isValidPassword('asdfpasdfpoijpassword')

