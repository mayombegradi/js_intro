let convertFahrenheit = function (fahrenheit) {
    return {
        fahrenheit: fahrenheit,
        celsius: (fahrenheit - 32) * (5 / 9),
        kelvin: (fahrenheit - 32) * (5 / 9) + 273.15
    }
}

console.log(convertFahrenheit(74))