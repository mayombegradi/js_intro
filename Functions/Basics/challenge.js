let fahrenheitToCelsius = function (fahrenheit) {
    return (fahrenheit - 32) * (5 / 9)
}

console.log(fahrenheitToCelsius(32))