let age = 29

// Null allows us to reset a variable value.
age = null

console.log(age)