let name

name = 'Jen'
if (name === undefined) {
    console.log('Please provide a name')
} else {
    console.log(name)
}

let square = function (num) {
    // This function returns undefined as
    // It returns nothing.
    console.log(num)
}

square()  // Prints undefined.
square(3) // Prints 3

result = square(3)
console.log(result)