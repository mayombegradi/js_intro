// Student's score, total possible score
let getGrade = function (maximum, obtainedScore = 0) {
    let percent = (obtainedScore * 100) / maximum

    if (percent >= 90) {
        return `You got an A (${percent})`
    } else if (percent >= 80) {
        return `You got an B (${percent})`
    } else if (percent >= 70) {
        return `You got an C (${percent})`
    } else if (percent >= 60) {
        return `You got an D (${percent})`
    } else {
        return `You got an F (${percent})`
    }
}

console.log(getGrade(20, 20))
console.log(getGrade(20, 17))
console.log(getGrade(20, 12))
console.log(getGrade(20, 2))