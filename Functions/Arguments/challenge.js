let getTip = function (total, tipPercent = .2) {
    return total * tipPercent
}

let tip = getTip(100) // 20 = 100 * .2
console.log(tip)

tip = getTip(100, .25) // 25 = 100 * .25
console.log(tip)