// Default Arguments
let getScoreText = function (name = 'Anonymous', score = 0) {
    return 'Name: ' + name + ' Score: ' + score
}
let result

result = getScoreText() // returns Anonymous and 0
console.log(result)

result = getScoreText('Gradie')
console.log(result)

result = getScoreText(undefined, 10)
console.log(result)